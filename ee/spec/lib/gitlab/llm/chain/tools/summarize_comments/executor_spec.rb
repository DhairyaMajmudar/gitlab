# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Llm::Chain::Tools::SummarizeComments::Executor, feature_category: :duo_chat do
  let_it_be(:user) { create(:user) }

  let(:ai_request_double) { instance_double(Gitlab::Llm::Chain::Requests::AiGateway) }
  let(:input) { 'input' }
  let(:options) { { input: input } }
  let(:command) { nil }
  let(:command_name) { '/summarize_comments' }
  let(:prompt_class) { Gitlab::Llm::Chain::Tools::SummarizeComments::Prompts::Anthropic }
  let_it_be_with_reload(:group) { create(:group) }
  let_it_be(:project) { create(:project, group: group) }
  let_it_be(:issue) { create(:issue, project: project) }
  let_it_be(:note) { create(:note_on_issue, project: project, noteable: issue) }
  let(:resource) { issue }
  let(:context) do
    Gitlab::Llm::Chain::GitlabContext.new(
      current_user: user, container: nil, resource: resource, ai_request: ai_request_double
    )
  end

  let(:expected_slash_commands) do
    {
      '/summarize_comments' => {
        description: 'Summarize issue comments.',
        selected_code_without_input_instruction: 'Summarize issue comments.',
        selected_code_with_input_instruction: "Summary of issue comments. Input: %<input>s."
      }
    }
  end

  subject(:tool) { described_class.new(context: context, options: {}) }

  before_all do
    group.add_developer(user)
  end

  describe '#name' do
    it 'returns tool name' do
      expect(described_class::NAME).to eq('SummarizeComments')
    end
  end

  describe '#execute' do
    context 'when context is authorized' do
      include_context 'with stubbed LLM authorizer', allowed: true

      before do
        allow(Ability).to receive(:allowed?).and_call_original
        allow(Ability).to receive(:allowed?).with(user, :summarize_comments, resource).and_return(true)
        allow(tool).to receive(:provider_prompt_class).and_return(prompt_class)
      end

      it 'calls prompt with correct params' do
        expect(prompt_class).to receive(:prompt).with(a_hash_including(:notes_content))

        tool.execute
      end

      it 'builds the expected prompt' do
        prompt = tool.prompt[:prompt]

        expected_prompt = <<~PROMPT.chomp
          You are an assistant that extracts the most important information from the comments in maximum 10 bullet points.
        PROMPT

        system_prompt = prompt[0][:content]
        user_prompt = prompt[1][:content]
        expect(system_prompt).to include(expected_prompt)
        expect(user_prompt).to include(note.note)
      end

      it 'sets the correct unit primitive' do
        allow(Gitlab::Llm::Chain::Requests::AiGateway).to receive(:new).with(user, {
          service_name: :summarize_comments,
          tracking_context: {
            request_id: nil,
            action: 'summarize_comments'
          }
        }).and_return(ai_request_double)
        expect(ai_request_double).to receive(:request).with(tool.prompt, unit_primitive: 'summarize_comments')

        tool.execute
      end

      context 'when issue does not contain any notes' do
        let_it_be(:issue1) { create(:issue, project: project) }
        let(:resource) { issue1 }

        it 'returns error answer' do
          expect(tool.execute.content)
            .to eq(_('This resource has no comments to summarize'))
        end
      end

      context 'when feature flag is disabled' do
        before do
          stub_feature_flags(summarize_notes_with_duo: false)
        end

        it 'returns error answer' do
          expect(tool.execute.content)
            .to eq(_('This feature is not enabled yet.'))
        end
      end

      context 'when response is successful' do
        before do
          allow(tool).to receive(:request).and_return('successful response')
        end

        it 'returns success answer' do
          expect(tool.execute.content).to eq('successful response')
        end
      end

      context 'when error is raised during a request' do
        before do
          allow(tool).to receive(:request).and_raise(StandardError)
        end

        it 'returns an error answer' do
          answer = tool.execute

          expect(answer.content).to eq("I'm sorry, I can't generate a response. Please try again.")
          expect(answer.error_code).to eq("M4000")
        end
      end
    end

    context 'when context is not authorized' do
      before do
        allow(Ability).to receive(:allowed?).with(user, :summarize_comments, resource).and_return(false)
      end

      it 'returns error answer' do
        answer = tool.execute

        response = "I'm sorry, I can't generate a response. You might want to try again. " \
          "You could also be getting this error because the items you're asking about " \
          "either don't exist, you don't have access to them, or your session has expired."
        expect(answer.content).to eq(response)
        expect(answer.error_code).to eq("M3003")
      end
    end
  end
end
