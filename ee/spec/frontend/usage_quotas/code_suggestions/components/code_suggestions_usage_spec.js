import { GlBadge } from '@gitlab/ui';
import Vue from 'vue';
import VueApollo from 'vue-apollo';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import * as Sentry from '~/sentry/sentry_browser_wrapper';
import { sprintf } from '~/locale';
import addOnPurchaseQuery from 'ee/usage_quotas/add_on/graphql/get_add_on_purchase.query.graphql';
import addOnPurchasesQuery from 'ee/usage_quotas/add_on/graphql/get_add_on_purchases.query.graphql';
import getCurrentLicense from 'ee/admin/subscriptions/show/graphql/queries/get_current_license.query.graphql';
import CodeSuggestionsIntro from 'ee/usage_quotas/code_suggestions/components/code_suggestions_intro.vue';
import CodeSuggestionsInfo from 'ee/usage_quotas/code_suggestions/components/code_suggestions_info_card.vue';
import CodeSuggestionsStatisticsCard from 'ee/usage_quotas/code_suggestions/components/code_suggestions_usage_statistics_card.vue';
import SaasAddOnEligibleUserList from 'ee/usage_quotas/code_suggestions/components/saas_add_on_eligible_user_list.vue';
import SelfManagedAddOnEligibleUserList from 'ee/usage_quotas/code_suggestions/components/self_managed_add_on_eligible_user_list.vue';
import HealthCheckList from 'ee/usage_quotas/code_suggestions/components/health_check_list.vue';
import CodeSuggestionsUsage from 'ee/usage_quotas/code_suggestions/components/code_suggestions_usage.vue';
import { useFakeDate } from 'helpers/fake_date';
import createMockApollo from 'helpers/mock_apollo_helper';
import waitForPromises from 'helpers/wait_for_promises';
import {
  CODE_SUGGESTIONS_TITLE,
  DUO_ENTERPRISE_TITLE,
} from 'ee/usage_quotas/code_suggestions/constants';
import {
  ADD_ON_ERROR_DICTIONARY,
  ADD_ON_PURCHASE_FETCH_ERROR_CODE,
} from 'ee/usage_quotas/error_constants';

import {
  subscriptionActivationFutureDatedNotificationTitle,
  SUBSCRIPTION_ACTIVATION_SUCCESS_EVENT,
} from 'ee/admin/subscriptions/show/constants';

import {
  noAssignedDuoProAddonData,
  deprecatedNoAssignedDuoProAddonData,
  noAssignedDuoEnterpriseAddonData,
  deprecatedNoAssignedDuoEnterpriseAddonData,
  noAssignedDuoAddonsData,
  noPurchasedAddonData,
  deprecatedNoPurchasedAddonData,
  purchasedAddonFuzzyData,
  deprecatedPurchasedAddonFuzzyData,
  currentLicenseData,
} from '../mock_data';

Vue.use(VueApollo);

jest.mock('~/sentry/sentry_browser_wrapper');
jest.mock('ee/usage_quotas/code_suggestions/components/health_check_list.vue', () => ({
  name: 'HealthCheckList',
  template: '<div></div>',
  methods: {
    runHealthCheck: jest.fn(),
  },
}));

describe('GitLab Duo Usage', () => {
  // Aug 1, 2024
  useFakeDate(2024, 8, 1);

  let wrapper;

  const error = new Error('Something went wrong');

  const noAssignedAddonDataHandler = jest.fn().mockResolvedValue(noAssignedDuoProAddonData);
  const deprecatedNoAssignedAddonDataHandler = jest
    .fn()
    .mockResolvedValue(deprecatedNoAssignedDuoProAddonData);
  const noAssignedAddonErrorHandler = jest.fn().mockRejectedValue(error);

  const noAssignedEnterpriseAddonDataHandler = jest
    .fn()
    .mockResolvedValue(noAssignedDuoEnterpriseAddonData);
  const deprecatedNoAssignedEnterpriseAddonDataHandler = jest
    .fn()
    .mockResolvedValue(deprecatedNoAssignedDuoEnterpriseAddonData);

  const noAssignedDuoAddonsDataHandler = jest.fn().mockResolvedValue(noAssignedDuoAddonsData);
  const deprecatedNoAssignedDuoAddonsDataHandler = jest
    .fn()
    .mockResolvedValue(deprecatedNoAssignedDuoProAddonData);

  const noPurchasedAddonDataHandler = jest.fn().mockResolvedValue(noPurchasedAddonData);
  const deprecatedNoPurchasedAddonDataHandler = jest
    .fn()
    .mockResolvedValue(deprecatedNoPurchasedAddonData);

  const purchasedAddonFuzzyDataHandler = jest.fn().mockResolvedValue(purchasedAddonFuzzyData);
  const deprecatedPurchasedAddonFuzzyDataHandler = jest
    .fn()
    .mockResolvedValue(deprecatedPurchasedAddonFuzzyData);

  const currentLicenseDataHandler = jest.fn().mockResolvedValue(currentLicenseData);

  const purchasedAddonErrorHandler = jest.fn().mockRejectedValue(error);
  const deprecatedPurchasedAddonErrorHandler = jest.fn().mockRejectedValue(error);
  const currentLicenseErrorHandler = jest.fn().mockRejectedValue(error);

  const createMockApolloProvider = ({
    addOnPurchaseHandler = deprecatedNoPurchasedAddonDataHandler,
    addOnPurchasesHandler = noPurchasedAddonDataHandler,
    currentLicenseHandler = currentLicenseDataHandler,
  }) => {
    return createMockApollo([
      [addOnPurchaseQuery, addOnPurchaseHandler],
      [addOnPurchasesQuery, addOnPurchasesHandler],
      [getCurrentLicense, currentLicenseHandler],
    ]);
  };

  const findCodeSuggestionsIntro = () => wrapper.findComponent(CodeSuggestionsIntro);
  const findCodeSuggestionsInfo = () => wrapper.findComponent(CodeSuggestionsInfo);
  const findCodeSuggestionsStatistics = () => wrapper.findComponent(CodeSuggestionsStatisticsCard);
  const findCodeSuggestionsSubtitle = () => wrapper.findByTestId('code-suggestions-subtitle');
  const findCodeSuggestionsTitle = () => wrapper.findByTestId('code-suggestions-title');
  const findCodeSuggestionsTitleTierBadge = () => wrapper.findComponent(GlBadge);
  const findSaasAddOnEligibleUserList = () => wrapper.findComponent(SaasAddOnEligibleUserList);
  const findHealthCheckList = () => wrapper.findComponent(HealthCheckList);
  const findSelfManagedAddOnEligibleUserList = () =>
    wrapper.findComponent(SelfManagedAddOnEligibleUserList);
  const findErrorAlert = () => wrapper.findByTestId('add-on-purchase-fetch-error');

  const findSubscriptionActivationSuccessAlert = () =>
    wrapper.findByTestId('subscription-activation-success-alert');

  const findSubscriptionFetchErrorAlert = () =>
    wrapper.findByTestId('subscription-fetch-error-alert');

  const createComponent = ({
    addOnPurchaseHandler,
    addOnPurchasesHandler,
    currentLicenseHandler,
    provideProps,
  } = {}) => {
    wrapper = shallowMountExtended(CodeSuggestionsUsage, {
      provide: {
        isSaaS: true,
        ...provideProps,
      },
      apolloProvider: createMockApolloProvider({
        addOnPurchaseHandler,
        addOnPurchasesHandler,
        currentLicenseHandler,
      }),
    });

    return waitForPromises();
  };

  describe('Cloud Connector health status check', () => {
    it.each`
      description   | isSaaS   | isStandalonePage | expected
      ${'does not'} | ${true}  | ${true}          | ${false}
      ${'does not'} | ${true}  | ${false}         | ${false}
      ${'does'}     | ${false} | ${true}          | ${true}
      ${'does'}     | ${false} | ${false}         | ${true}
    `(
      '$description render the health check list with isSaaS is $isSaaS, and isStandalonePage is $isStandalonePage',
      async ({ isSaaS, isStandalonePage, expected } = {}) => {
        createComponent({
          addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
          addOnPurchasesHandler: noAssignedAddonDataHandler,
          provideProps: {
            isStandalonePage,
            isSaaS,
          },
        });
        await waitForPromises();

        expect(findHealthCheckList().exists()).toBe(expected);
      },
    );
  });

  describe('when no group id prop is provided', () => {
    beforeEach(() => {
      createComponent({
        addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
        addOnPurchasesHandler: noAssignedAddonDataHandler,
      });
    });

    it('calls addOnPurchases query with appropriate props', () => {
      expect(noAssignedAddonDataHandler).toHaveBeenCalledWith({
        namespaceId: null,
      });
    });

    it('does not call addOnPurchase query', () => {
      expect(deprecatedNoAssignedAddonDataHandler).not.toHaveBeenCalled();
    });
  });

  describe('when group id prop is provided', () => {
    beforeEach(() => {
      createComponent({
        addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
        addOnPurchasesHandler: noAssignedAddonDataHandler,
        provideProps: { groupId: '289561' },
      });
    });

    it('calls addOnPurchases query with appropriate props', () => {
      expect(noAssignedAddonDataHandler).toHaveBeenCalledWith({
        namespaceId: 'gid://gitlab/Group/289561',
      });
    });

    it('does not call addOnPurchase query', () => {
      expect(deprecatedNoAssignedAddonDataHandler).not.toHaveBeenCalled();
    });
  });

  describe('with no code suggestions data', () => {
    describe('when instance is SaaS', () => {
      beforeEach(() => {
        return createComponent();
      });

      it('does not render code suggestions title', () => {
        expect(findCodeSuggestionsTitle().exists()).toBe(false);
      });

      it('does not render code suggestions subtitle', () => {
        expect(findCodeSuggestionsSubtitle().exists()).toBe(false);
      });

      it('renders code suggestions intro', () => {
        expect(findCodeSuggestionsIntro().exists()).toBe(true);
      });
    });

    describe('when instance is SM', () => {
      beforeEach(() => {
        return createComponent({ provideProps: { isSaaS: false } });
      });

      it('does not render code suggestions title', () => {
        expect(findCodeSuggestionsTitle().exists()).toBe(false);
      });

      it('does not render code suggestions subtitle', () => {
        expect(findCodeSuggestionsSubtitle().exists()).toBe(false);
      });

      it('renders code suggestions intro', () => {
        expect(findCodeSuggestionsIntro().exists()).toBe(true);
      });
    });
  });

  describe('with code suggestions data', () => {
    describe('when instance is SaaS', () => {
      describe('when on the `Usage Quotas` page', () => {
        beforeEach(() => {
          return createComponent({
            addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
            addOnPurchasesHandler: noAssignedAddonDataHandler,
            provideProps: { groupId: '289561' },
          });
        });

        it('does not render code suggestions title', () => {
          expect(findCodeSuggestionsTitle().exists()).toBe(false);
        });

        it('does not render code suggestions subtitle', () => {
          expect(findCodeSuggestionsSubtitle().exists()).toBe(false);
        });

        it('does not render code suggestions intro', () => {
          expect(findCodeSuggestionsIntro().exists()).toBe(false);
        });
      });

      describe('when on the standalone page', () => {
        beforeEach(() => {
          return createComponent({
            addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
            addOnPurchasesHandler: noAssignedAddonDataHandler,
            provideProps: { isStandalonePage: true, groupId: '289561' },
          });
        });

        it('renders code suggestions title and pro tier badge', () => {
          expect(findCodeSuggestionsTitle().text()).toBe('GitLab Duo');
          expect(findCodeSuggestionsTitleTierBadge().text()).toBe('pro');
        });

        it('renders code suggestions subtitle', () => {
          expect(findCodeSuggestionsSubtitle().text()).toBe(
            sprintf('Manage seat assignments for %{addOnName}.', {
              addOnName: CODE_SUGGESTIONS_TITLE,
            }),
          );
        });
      });

      describe('with Duo Pro add-on enabled', () => {
        describe('when getAddOnPurchases endpoint is available', () => {
          beforeEach(() => {
            return createComponent({
              addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
              addOnPurchasesHandler: noAssignedAddonDataHandler,
              provideProps: { groupId: '289561' },
            });
          });

          it('renders code suggestions statistics card for duo pro', () => {
            expect(findCodeSuggestionsStatistics().props()).toEqual({
              usageValue: 0,
              totalValue: 20,
              duoTier: 'pro',
            });
          });

          it('renders code suggestions info card for duo pro', () => {
            expect(findCodeSuggestionsInfo().exists()).toBe(true);
            expect(findCodeSuggestionsInfo().props()).toEqual({
              groupId: '289561',
              duoTier: 'pro',
            });
          });
        });

        describe('when getAddOnPurchases endpoint is not available', () => {
          beforeEach(() => {
            return createComponent({
              addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
              addOnPurchasesHandler: noAssignedAddonErrorHandler,
              provideProps: { groupId: '289561' },
            });
          });

          it('falls back on deprecated getAddOnPurchase endpoint', () => {
            expect(deprecatedNoAssignedAddonDataHandler).toHaveBeenCalled();

            expect(findCodeSuggestionsInfo().exists()).toBe(true);
            expect(findCodeSuggestionsInfo().props()).toEqual({
              groupId: '289561',
              duoTier: 'pro',
            });
          });
        });
      });

      describe('with Duo Enterprise add-on enabled', () => {
        beforeEach(() => {
          return createComponent({
            addOnPurchaseHandler: deprecatedNoAssignedEnterpriseAddonDataHandler,
            addOnPurchasesHandler: noAssignedEnterpriseAddonDataHandler,
            provideProps: { groupId: '289561' },
          });
        });

        it('renders code suggestions statistics card for duo enterprise', () => {
          expect(findCodeSuggestionsStatistics().props()).toEqual({
            usageValue: 0,
            totalValue: 20,
            duoTier: 'enterprise',
          });
        });

        it('renders code suggestions info card for duo enterprise', () => {
          expect(findCodeSuggestionsInfo().exists()).toBe(true);
          expect(findCodeSuggestionsInfo().props()).toEqual({
            groupId: '289561',
            duoTier: 'enterprise',
          });
        });
      });

      describe('with both Duo Pro and Enterprise add-ons enabled', () => {
        beforeEach(() => {
          return createComponent({
            addOnPurchaseHandler: deprecatedNoAssignedDuoAddonsDataHandler,
            addOnPurchasesHandler: noAssignedDuoAddonsDataHandler,
            provideProps: { groupId: '289561' },
          });
        });

        it('renders addon user list for duo enterprise', () => {
          expect(findSaasAddOnEligibleUserList().props()).toEqual({
            addOnPurchaseId: 'gid://gitlab/GitlabSubscriptions::AddOnPurchase/4',
            duoTier: 'enterprise',
          });
        });

        it('renders code suggestions statistics card for duo enterprise', () => {
          expect(findCodeSuggestionsStatistics().props()).toEqual({
            usageValue: 0,
            totalValue: 20,
            duoTier: 'enterprise',
          });
        });

        it('renders code suggestions info card for duo enterprise', () => {
          expect(findCodeSuggestionsInfo().exists()).toBe(true);
          expect(findCodeSuggestionsInfo().props()).toEqual({
            groupId: '289561',
            duoTier: 'enterprise',
          });
        });
      });
    });

    describe('when instance is SM', () => {
      beforeEach(() => {
        return createComponent({
          addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
          addOnPurchasesHandler: noAssignedAddonDataHandler,
          provideProps: { isSaaS: false },
        });
      });

      it('renders code suggestions title and pro tier badge', () => {
        expect(findCodeSuggestionsTitle().text()).toBe('GitLab Duo');
        expect(findCodeSuggestionsTitleTierBadge().text()).toBe('pro');
      });

      it('renders code suggestions subtitle', () => {
        expect(findCodeSuggestionsSubtitle().text()).toBe(
          sprintf(
            'Manage seat assignments for %{addOnName} or run a health check to identify problems.',
            {
              addOnName: CODE_SUGGESTIONS_TITLE,
            },
          ),
        );
      });

      describe('with Duo Enterprise add-on enabled', () => {
        beforeEach(() => {
          return createComponent({
            addOnPurchaseHandler: deprecatedNoAssignedEnterpriseAddonDataHandler,
            addOnPurchasesHandler: noAssignedEnterpriseAddonDataHandler,
            provideProps: { isSaaS: false },
          });
        });

        it('renders code suggestions title and enterprise tier badge', () => {
          expect(findCodeSuggestionsTitle().text()).toBe('GitLab Duo');
          expect(findCodeSuggestionsTitleTierBadge().text()).toBe('enterprise');
        });

        it('renders code suggestions subtitle', () => {
          expect(findCodeSuggestionsSubtitle().text()).toBe(
            sprintf(
              'Manage seat assignments for %{addOnName} or run a health check to identify problems.',
              {
                addOnName: DUO_ENTERPRISE_TITLE,
              },
            ),
          );
        });
      });

      it('does not render code suggestions intro', () => {
        expect(findCodeSuggestionsIntro().exists()).toBe(false);
      });

      it('renders code suggestions statistics card', () => {
        expect(findCodeSuggestionsStatistics().props()).toEqual({
          usageValue: 0,
          totalValue: 20,
          duoTier: 'pro',
        });
      });

      it('renders code suggestions info card', () => {
        expect(findCodeSuggestionsInfo().exists()).toBe(true);
      });
    });
  });

  describe('add on eligible user list', () => {
    it('renders addon user list for SaaS instance for SaaS', async () => {
      createComponent({
        addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
        addOnPurchasesHandler: noAssignedAddonDataHandler,
        provideProps: { isSaaS: true },
      });
      await waitForPromises();

      expect(findSaasAddOnEligibleUserList().props()).toEqual({
        addOnPurchaseId: 'gid://gitlab/GitlabSubscriptions::AddOnPurchase/3',
        duoTier: 'pro',
      });
    });

    it('renders addon user list for SM instance for SM', async () => {
      createComponent({
        addOnPurchaseHandler: deprecatedNoAssignedAddonDataHandler,
        addOnPurchasesHandler: noAssignedAddonDataHandler,
        provideProps: { isSaaS: false },
      });
      await waitForPromises();

      expect(findSelfManagedAddOnEligibleUserList().props()).toEqual({
        addOnPurchaseId: 'gid://gitlab/GitlabSubscriptions::AddOnPurchase/3',
        duoTier: 'pro',
      });
    });
  });

  describe('with fuzzy code suggestions data', () => {
    beforeEach(() => {
      return createComponent({
        addOnPurchaseHandler: deprecatedPurchasedAddonFuzzyDataHandler,
        addOnPurchasesHandler: purchasedAddonFuzzyDataHandler,
      });
    });

    it('renders code suggestions intro', () => {
      expect(findCodeSuggestionsIntro().exists()).toBe(true);
    });
  });

  describe('with errors', () => {
    describe('when instance is SaaS', () => {
      beforeEach(() => {
        return createComponent({
          addOnPurchaseHandler: deprecatedPurchasedAddonErrorHandler,
          addOnPurchasesHandler: purchasedAddonErrorHandler,
        });
      });

      it('does not render code suggestions title', () => {
        expect(findCodeSuggestionsTitle().exists()).toBe(false);
      });

      it('does not render code suggestions subtitle', () => {
        expect(findCodeSuggestionsSubtitle().exists()).toBe(false);
      });

      it('does not render code suggestions intro', () => {
        expect(findCodeSuggestionsIntro().exists()).toBe(false);
      });

      it('captures the original error', () => {
        expect(Sentry.captureException).toHaveBeenCalledTimes(1);
        expect(Sentry.captureException).toHaveBeenCalledWith(error, {
          tags: { vue_component: 'CodeSuggestionsUsage' },
        });
      });

      it('shows an error alert with cause', () => {
        expect(findErrorAlert().props('errorDictionary')).toMatchObject(ADD_ON_ERROR_DICTIONARY);
        const caughtError = findErrorAlert().props('error');
        expect(caughtError.cause).toBe(ADD_ON_PURCHASE_FETCH_ERROR_CODE);
      });
    });

    describe('when instance is SM', () => {
      beforeEach(() => {
        return createComponent({
          addOnPurchaseHandler: deprecatedPurchasedAddonErrorHandler,
          addOnPurchasesHandler: purchasedAddonErrorHandler,
          provideProps: { isSaaS: false },
        });
      });

      it('renders code suggestions title and pro tier badge', () => {
        expect(findCodeSuggestionsTitle().text()).toBe('GitLab Duo');
        expect(findCodeSuggestionsTitleTierBadge().text()).toBe('pro');
      });

      it('renders code suggestions subtitle', () => {
        expect(findCodeSuggestionsSubtitle().text()).toBe(
          sprintf(
            'Manage seat assignments for %{addOnName} or run a health check to identify problems.',
            {
              addOnName: CODE_SUGGESTIONS_TITLE,
            },
          ),
        );
      });

      it('does not render code suggestions intro', () => {
        expect(findCodeSuggestionsIntro().exists()).toBe(false);
      });

      it('captures the original error', () => {
        expect(Sentry.captureException).toHaveBeenCalledTimes(1);
        expect(Sentry.captureException).toHaveBeenCalledWith(error, {
          tags: { vue_component: 'CodeSuggestionsUsage' },
        });
      });

      it('shows an error alert with cause', () => {
        expect(findErrorAlert().props('errorDictionary')).toMatchObject(ADD_ON_ERROR_DICTIONARY);
        const caughtError = findErrorAlert().props('error');
        expect(caughtError.cause).toBe(ADD_ON_PURCHASE_FETCH_ERROR_CODE);
      });
    });
  });

  describe('Subscription Activation Form', () => {
    describe('activating the license', () => {
      beforeEach(async () => {
        createComponent({ currentLicenseHandler: currentLicenseDataHandler });

        await waitForPromises();
      });

      it('passes the correct data to the code suggestions intro', () => {
        expect(findCodeSuggestionsIntro().props()).toMatchObject({
          subscription: currentLicenseData.data.currentLicense,
        });
      });

      it('shows the activation success notification', async () => {
        findCodeSuggestionsIntro().vm.$emit(SUBSCRIPTION_ACTIVATION_SUCCESS_EVENT, {
          startsAt: '2024-08-01',
        });

        await waitForPromises();

        expect(findSubscriptionActivationSuccessAlert().props('title')).toBe(
          'Your subscription was successfully activated.',
        );
      });

      it('shows the future dated activation success notification', async () => {
        findCodeSuggestionsIntro().vm.$emit(SUBSCRIPTION_ACTIVATION_SUCCESS_EVENT, {
          startsAt: '2025-08-01',
        });

        await waitForPromises();

        expect(findSubscriptionActivationSuccessAlert().props('title')).toBe(
          subscriptionActivationFutureDatedNotificationTitle,
        );
      });

      it('calls refetch to update component', async () => {
        findCodeSuggestionsIntro().vm.$emit(SUBSCRIPTION_ACTIVATION_SUCCESS_EVENT, {
          startsAt: '2025-08-01',
        });

        await waitForPromises();

        expect(noPurchasedAddonDataHandler).toHaveBeenCalledTimes(2);
        expect(currentLicenseDataHandler).toHaveBeenCalledTimes(2);
      });
    });

    describe('when fetch subscription with error', () => {
      beforeEach(async () => {
        createComponent({ currentLicenseHandler: currentLicenseErrorHandler });

        await waitForPromises();
      });

      it('shows an error alert with cause', () => {
        expect(findSubscriptionFetchErrorAlert().props('title')).toBe('Subscription unavailable');
      });
    });
  });
});
